import pytest
from decurtis.ui.web_ui_commons import WebUICommons

@pytest.fixture(scope="session")
def session_level():
    print('executes before and after entire test suite')

@pytest.fixture(scope="module")
def module_level():
    print('executes before and after each test module')
    WebUICommons().launch_browser(browser='chrome', grid=False)
    # yield
    # WebUICommons().close_browser()

@pytest.fixture(scope="class")
def class_level():
    print('executes before and after each class')

@pytest.fixture(scope="function")
def method_level():
    print('executes before and after each test method')


