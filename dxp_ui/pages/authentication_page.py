from decurtis.ui.web_ui_commons import WebUICommons
from decurtis.common import *
import json

class AuthenticationPage(WebUICommons):
    def __init__(self):
        with open(os.path.join('dxp_ui/json_files', 'authentication_page.json')) as auth :
            self.locators = json.load(auth)

    def authentication(self, username, password):
        self.send_text(self.locators, 'username_xpath', username)
        self.send_text(self.locators, 'password_xpath', password)
        self.click(self.locators, 'let_me_in_btn_xpath')


