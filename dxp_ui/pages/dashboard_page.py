from decurtis.ui.web_ui_commons import WebUICommons
from decurtis.common import *

class DashboardPage(WebUICommons):
    def __init__(self):
        with open(os.path.join('dxp_ui/json_files', 'dashboard_page.json')) as dash :
            self.locators = json.load(dash)

    def click_signup_link(self):
        self.wait_till_element_present(self.locators, "signuplink_xpath")
        if(self.is_element_display_on_screen(self.locators, "signuplink_xpath")):
            self.wait_for(5)
            self.click(self.locators, "signuplink_xpath")