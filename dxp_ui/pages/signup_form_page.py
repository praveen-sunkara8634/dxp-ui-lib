from decurtis.ui.web_ui_commons import WebUICommons
from decurtis.common import *

class SignUpPage(WebUICommons):
    def __init__(self):
        with open(os.path.join('dxp_ui/json_files', 'signup_form_page.json')) as signup :
            self.locators = json.load(signup)

    def complete_registration(self, fname, lname, phone, email):
        self.send_text(self.locators, "fname_xpath", fname)
        self.send_text(self.locators, "lname_xpath", lname)
        self.send_text(self.locators, "phone_number_xpath", phone)
        self.clear_with_backspace(self.locators, "dob_xpath")
        self.send_text(self.locators, "dob_xpath", "02-02-1990")
        self.wait_for(2)
        self.click(self.locators, "email_xpath")
        self.send_text(self.locators, "email_xpath", email)
        self.send_text(self.locators, "password_xpath", "Yellow*99")
        self.send_text(self.locators, "confirm_password_xpath", "Yellow*99")
        self.scroll_and_click(self.locators, "tnc_checkbox_xpath")
        self.scroll_and_click(self.locators, "subscribe_checkbox_xpath")
        self.scroll_and_click(self.locators, "submit_button_xpath")
